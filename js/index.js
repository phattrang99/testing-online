/**
 * Project: Web thi trắc nghiệm
 * Chức năng:
 *    1 .Hiển thị danh sách câu hỏi
 *    2. Cho người dùng làm bài và tính điểm
 *
 * Giao diện:
 * Phân tích lớp đối tượng
 *
 */

import MultipleChoice from "../models/multipleChoice.js";
import FillInBlank from "../models/FillInBlank.js";

let questionList = [];

const fetchQuestions = () => {
  axios({
    method: "GET",
    url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/questions",
  })
    .then((res) => {
      console.log(res);
      questionList = mapData(res.data);
      renderQuestions(questionList);
    })
    .catch((err) => {
      console.log(err);
    });
};

const renderQuestions = (data) => {
  let htmlContent = "";
  for (const i in data) {
    htmlContent += data[i].render(+i + 1);
  }
  document.getElementById("content").innerHTML = htmlContent;
};

const mapData = (data) => {
  const mappedData = data.map((item) => {
    const { questionType, id, content, answers } = item;
    if (questionType === 1) {
      return new MultipleChoice(content, id, questionType, answers);
    }
    return new FillInBlank(content, id, questionType, answers);
  });

  return mappedData;
};

const handleSubmit = () => {
  let result = 0;
  for (const item of questionList) {
    if (item.checkExact()) {
      result++;
    }
  }
  alert(`Tổng số câu đúng: ${result}/${questionList.length}`);
};

document.getElementById("btnSubmit").onclick = handleSubmit;

fetchQuestions();
