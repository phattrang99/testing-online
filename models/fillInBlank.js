import Question from "./question.js";

class FillInBlank extends Question {
  constructor(content, id, type, answers) {
    // hàm super sẽ giúp kế thừa lại constructor() của
    // Question => kế thừa các thuộc tính của Question
    super(type, id, content, answers);
    // bổ sung thêm 1 thuộc tính name cho riêng lớp
    // FillInBlank

    // this.name = name;
  }

  checkExact() {
    const answer = document.getElementById("answer-" + this.id).value;
    if (this.answers[0] === undefined) {
      return;
    }
    return answer.toLowerCase() === this.answers[0].content.toLowerCase();
  }

  // return về giao diện html của câu hỏi
  render(index) {
    return `
        <div>
            <h1>Câu hỏi ${index}: ${this.content}</h1>
            <input id="answer-${this.id}" />
        </div>
      `;
  }
}

export default FillInBlank;
