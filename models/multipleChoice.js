import Question from "./question.js";

class MultipleChoice extends Question {
  constructor(content, id, type, answers) {
    // hàm super sẽ giúp kế thừa lại constructor() của
    // Question => kế thừa các thuộc tính của Question
    super(type, id, content, answers);
    // bổ sung thêm 1 thuộc tính name cho riêng lớp
    // MultipleChoice

    // this.name = name;
  }

  checkExact() {
    // 1. check xem thí sinh đã chọn input nào (checked) => id đáp án (input.value)
    const answerInps = document.getElementsByClassName("answer-" + this.id);
    let answerId;

    const foundInput = [...answerInps].find((item) => {
      return item.checked;
    });

    // 2. kiểm tra thí sinh ko chọn đáp án => không tìm thấy input nào checked=> false
    if (!foundInput) return false;

    answerId = foundInput.value;

    // 3. dựa vào id, tìm ra đối tượng đáp án trong danh sách => đối tượng đáp án => exact
    const foundAnswer = this.answers.find((item) => {
      return item.id === answerId;
    });

    return foundAnswer.exact;
  }

  // return về giao diện html của câu hỏi
  render(index) {
    let answersHTML = "";

    for (const item of this.answers) {
      answersHTML += `
            <div>
                <input type="radio" class="answer-${this.id}" value="${item.id}" name="answer-${this.id}" />
                <label>${item.content}</label>
            </div>
        `;
    }

    return `
        <div>
            <h1>Câu hỏi ${index}: ${this.content}</h1>
            ${answersHTML}
        </div>
      `;
  }
}

export default MultipleChoice;
