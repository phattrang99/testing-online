class Question {
  //phương thức khởi tạo, mặc định có sẵn trong class
  constructor(type, id, content, answers) {
    this.questionType = type;
    this.id = id;
    this.content = content;
    this.answers = answers;
  }
  checkExact() {}
  render() {}
}

export default Question;
