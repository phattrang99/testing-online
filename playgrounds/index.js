// ----MAP----
const students = [
  { name: "hieu", age: 12 },
  { name: "dung", age: 14 },
  { name: "tai", age: 15 },
  { name: "quynh", age: 16 },
];

const studentNames = students.map((item) => {
  return item.name;
});

//------FIND------
console.log(
  students.find((item) => {
    return item.name === "hieu";
  })
);

//------FINDINDEX------
console.log(
  students.findIndex((item) => {
    return item.name === "hieu";
  })
);

// ---FILTER----
console.log(
  students.filter((item) => {
    return item.age >= 15;
  })
);

console.log(studentNames);

// => ["hieu", "dung", "tai", "quynh"]

const nums = [1, 2, 3, 4];

const nums2 = nums.map((x) => {
  return x * 2;
});

console.log(nums2);

// => [2,4,6,8]
